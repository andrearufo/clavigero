<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

    Route::post('/logout', 'AuthController@logout');
    Route::get('/user', 'AuthController@user');

    Route::get('/users', 'UsersController@index');
    Route::post('/users/store', 'UsersController@store');

    Route::prefix('credentials')->group(function () {
        Route::get('index', 'CredentialController@index');
        Route::post('store', 'CredentialController@store');
        Route::get('show/{credential}', 'CredentialController@show');
        Route::post('destroy/{credential}', 'CredentialController@destroy');
        Route::post('search', 'CredentialController@search');
    });

    Route::prefix('votes')->group(function () {
        Route::post('store', 'VoteController@store');
    });

});

Route::post('/login', 'AuthController@login')->name('login');
Route::post('/register', 'AuthController@register');

// Route::get('/import', 'AuthController@import');

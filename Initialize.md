# How to initialize the project

* Clone the repo
* Set correct permission to file and folders running

```
sudo chown -R www-data:www-data /var/www/clavigero.andrearufo.it/
sudo find /var/www/clavigero.andrearufo.it -type f -exec chmod 644 {} \;
sudo find /var/www/clavigero.andrearufo.it -type d -exec chmod 755 {} \;
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
```

* Run `composer install`
* Copy the `.env-example` running `cp .env-example .env` or...
* ...create a `.env` file in root and edit the `.env` file coping the `.env-example` content
* Compiling the info about app and database
* Run `php artisan key:generate`
* Create the database structure using the migrations running `php artisan migrate`
* Run `php artisan passport:install`
* Copy the `Client ID` and `Client secret` into `PASSPORT_CLIENT_ID` and `PASSPORT_CLIENT_SECRET` in the `.env` file
* Use tinker to create a new user: run `php artisan tinker`
* And insert:

```
$user = new User;
$user->email = 'your@email.com';
$user->name = 'yourname';
$user->password = Hash::make('password');
$user->save();
```

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        user: localStorage.getItem('user') || null,
        token: localStorage.getItem('access_token') || null,
        online: true,
        flash: {
            type: false,
            info: false
        }
    },
    getters: {
        loggedIn(state){
            return state.token !== null;
        },
        currentUser(state){
            return JSON.parse(state.user);
        }
    },
    mutations: {
        retriveToken(state, token){
            state.token = token;
        },
        destroyToken(state){
            state.token = null;
        },
        retriveCurrentUser(state, user){
            state.user = user;
        },
        destroyCurrentUser(state){
            state.user = null;
        },
        retriveFlash(state, data){
            state.flash.type = 'danger';
            state.flash.info = data;
        },
        destroyFlash(state){
            console.log('Falsh destroied');
            state.flash.type = false;
            state.flash.info = false;
        }
    },
    actions: {
        login(context, credentials){
            return new Promise((resolve, reject) => {
                axios.post('/api/login',{
                    username: credentials.username,
                    password: credentials.password
                }).then((response) => {
                    console.log(response.data);
                    
                    const token = response.data.access_token;
                    localStorage.setItem('access_token', token);
                    context.commit('retriveToken', token);
                    resolve(response);
                }).catch((error) => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        logout(context) {
            if( context.getters.loggedIn ){
                axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve, reject) => {
                    axios.post('/api/logout').then(response => {
                        localStorage.removeItem('access_token');
                        context.commit('destroyToken');
                        resolve(response);
                    }).catch(error => {
                        localStorage.removeItem('access_token');
                        context.commit('destroyToken');
                        context.commit('retriveFlash', error.response.data);
                        reject(error);
                    });
                });
            }
        },
        getCurrentUser(context){
            if( context.getters.loggedIn ){
                console.log('Current User in arrivo');
                axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve, reject) => {
                    axios.get('/api/user').then(response => {
                        const user = JSON.stringify(response.data);
                        console.log(user);
                        localStorage.setItem('user', user);
                        context.commit('retriveCurrentUser', user);
                        resolve(response);
                    }).catch(error => {
                        context.commit('retriveFlash', error.response.data);
                        reject(error);
                    });
                });
            }
        },
        getUsers(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.get('/api/users').then(response => {
                    resolve(response);
                }).catch(error => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        storeUser(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.post('/api/users/store', {
                    name: data.name,
                    email: data.email,
                    password: data.password
                }).then(response => {
                    resolve(response);
                }).catch(error => {
                    console.log(error.response);
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        getCredentials(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.get('/api/credentials/index').then(response => {
                    resolve(response);
                }).catch(error => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        getCredential(context, data){
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.get('/api/credentials/show/'+data.id).then(response => {
                    resolve(response);
                }).catch(error => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        storeCredential(context, data){
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.post('/api/credentials/store', data).then(response => {
                    resolve(response);
                }).catch(error => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        searchCredentials(context, data){
            console.log('start');
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.post('/api/credentials/search', data).then(response => {
                    console.log(response);
                    resolve(response);
                }).catch(error => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
        storeVote(context, data){
            axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            return new Promise((resolve, reject) => {
                axios.post('/api/votes/store', data).then(response => {
                    resolve(response);
                }).catch(error => {
                    context.commit('retriveFlash', error.response.data);
                    reject(error);
                });
            });
        },
    },
    created(){
        console.log('Store is running');
    }
})

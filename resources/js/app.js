/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/

require('./bootstrap');

import Vue from 'vue'; // window.Vue = require('vue');
import VueRouter from 'vue-router';
import {store} from './store';

Vue.use(VueRouter);

const AppMain = Vue.component('app-main', require('./components/App/Main.vue').default);
const AppMenu = Vue.component('app-menu', require('./components/App/Menu.vue').default);
const AppFlash = Vue.component('app-flash', require('./components/App/Flash.vue').default);
const AppFastsearch = Vue.component('app-fastsearch', require('./components/App/Fastsearch.vue').default);

const Home = Vue.component('home', require('./components/Home.vue').default);
const Error404 = Vue.component('error404', require('./components/Error404.vue').default);
const Login = Vue.component('login', require('./components/Login.vue').default);
const Recover = Vue.component('recover', require('./components/Recover.vue').default);
const Logout = Vue.component('logout', require('./components/Logout.vue').default);
const Dashboard = Vue.component('dashboard', require('./components/Dashboard.vue').default);
const Search = Vue.component('search', require('./components/Search.vue').default);

const PageAbout = Vue.component('page-about', require('./components/Pages/About.vue').default);

const UsersIndex = Vue.component('users-index', require('./components/Users/Index.vue').default);
const UsersAdd = Vue.component('users-add', require('./components/Users/Add.vue').default);

const CredentialsIndex = Vue.component('credentials-home', require('./components/Credentials/Index.vue').default);
const CredentialsShow = Vue.component('credentials-show', require('./components/Credentials/Show.vue').default);
const CredentialsRevision = Vue.component('credentials-revision', require('./components/Credentials/Revision.vue').default);
const CredentialsAdd = Vue.component('credentials-add', require('./components/Credentials/Add.vue').default);
const CredentialsPartsMask = Vue.component('credentials-parts-mask', require('./components/Credentials/Parts/Mask.vue').default);
const CredentialsPartsSelectType = Vue.component('credentials-parts-selecttype', require('./components/Credentials/Parts/SelectType.vue').default);
const CredentialsPartsGrid = Vue.component('credentials-parts-grid', require('./components/Credentials/Parts/Grid.vue').default);
const CredentialsPartsResults = Vue.component('credentials-parts-results', require('./components/Credentials/Parts/Results.vue').default);

const GenericLoading = Vue.component('generic-loading', require('./components/Generic/Loading.vue').default);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/pages/about',
        name: 'about',
        component: PageAbout
    },
    {
        path: '/login/:error?',
        name: 'login',
        component: Login,
        meta: { unauth: true }
    },
    {
        path: '/recover',
        name: 'recover',
        component: Recover,
        meta: { unauth: true }
    },
    {
        path: '/logout',
        name: 'logout',
        component: Logout,
        meta: { auth: true }
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: { auth: true }
    },
    {
        path: '/search',
        name: 'search',
        component: Search,
        meta: { auth: true }
    },
    // Users
    {
        path: '/users',
        name: 'usersindex',
        component: UsersIndex,
        meta: { auth: true }
    },
    {
        path: '/users/add',
        name: 'usersadd',
        component: UsersAdd,
        meta: { auth: true }
    },
    // Credentials
    {
        path: '/credentials',
        name: 'credentialsindex',
        component: CredentialsIndex,
        meta: { auth: true }
    },
    {
        path: '/credentials/show/:id',
        name: 'credentialsshow',
        component: CredentialsShow,
        meta: { auth: true }
    },
    {
        path: '/credentials/revision/:id',
        name: 'credentialsrevision',
        component: CredentialsRevision,
        meta: { auth: true }
    },
    {
        path: '/credentials/add',
        name: 'credentialsadd',
        component: CredentialsAdd,
        meta: { auth: true }
    },

    {
        // will match everything
        path: '*',
        name: 'error404',
        component: Error404
    }
];

const router = new VueRouter({
    routes,
    // mode: 'history'
});

router.beforeEach((to, from, next) => {
    store.commit('destroyFlash');

    if(to.matched.some(record => record.meta.auth)){
        if(!store.getters.loggedIn){
            console.error('You must be logged in');
            next({ name: 'login', params: { error: 'unauth' } });
        }else{
            next();
        }
    }else if(to.matched.some(record => record.meta.unauth)){
        if(store.getters.loggedIn){
            console.error('You are just logged in');
            next({ name: 'dashboard' });
        }else{
            next();
        }
    }else{
        next();
    }
});

const app = new Vue({
    store,
    router,
    mode: 'history'
}).$mount('#app');

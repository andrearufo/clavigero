<?php

namespace App\Http\Controllers;

use App\Credential;
use Illuminate\Http\Request;

class CredentialController extends Controller
{

    public function index(Request $request)
    {
        return Credential::all();
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'type' => 'required|string|max:255',
            'infos' => 'required',
            'revision_of' => 'exists:credentials,id'
        ]);

        return Credential::create([
            'title' => $request->title,
            'type' => $request->type,
            'infos' => $request->infos,
            'user_id' => $request->user()->id,
            'revision_of' => $request->revision_of ?: null
        ]);
    }

    public function update(Credential $credential, Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'type' => 'required|string|max:255',
            'infos' => 'required',
            'revision_of' => 'exists:credentials,id'
        ]);

        $credential->title = $request->title;
        $credential->type = $request->type;
        $credential->infos = $request->infos;
        $credential->user_id = $request->user()->id;
        $credential->revision_of = $request->revision_of ?: null;

        return $credential->save();
    }

    public function show(Credential $credential)
    {
        return $credential;
    }

    public function destroy(Credential $credential)
    {
        $credential->delete();
        return void;
    }

    public function search(Request $request)
    {
        $request->validate([
            'key' => 'required'
        ]);

        $type = $request->type;
        $key = $request->key;

        $credentials = Credential::query();
        if ($key) {
            $credentials->where('title', 'like', '%'.$key.'%')->orwhere('infos', 'like', '%'.$key.'%');
        }
        if ($type) {
            $credentials->where('type', $type);
        }

        return $credentials->get();
    }
}

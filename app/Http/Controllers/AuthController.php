<?php

namespace App\Http\Controllers;

use App\User;
use App\Credential;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function user(Request $request)
    {
        return $request->user();
    }

    public function login(Request $request)
    {
        $http = new \GuzzleHttp\Client;
		$params = [
			'grant_type' => 'password',
			'client_id' => config('services.passport.client_id'),
			'client_secret' => config('services.passport.client_secret'),
			'username' => $request->username,
			'password' => $request->password,
		];
		return $params;

        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => $params
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }
            return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            // $token->revoke();
            $token->delete();
        });
        return response()->json('Logged out successfully', 200);
    }

    public function import()
    {
        return;

        $dir = $_SERVER['DOCUMENT_ROOT'].'/../ftp_ago/';

        // echo '<pre>'.print_r($dir, 1).'</pre>'; exit;

        $thelist = [];
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle)))
            {
                if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'xml')
                {
                    $xml = simplexml_load_file($dir.$file);
                    $thelist[] = [
                        'nome' => $file,
                        // 'content' => file_get_contents($dir.$file),
                        'data' => (array) $xml->Servers->Server
                    ];
                }
            }
            closedir($handle);
        }
        // return $thelist;

        $creds = [];
        foreach($thelist as $k => $item){
            $cred = [
                'title' => $item['data']['Name'],
                'type' => $item['data']['Protocol'] == 1 ? 'ssh' : 'ftp',
                'infos' => [
                    'host' => $item['data']['Host'],
                    'username' => $item['data']['User'],
                    'password' => $item['data']['Pass'],
                    'note' => 'Import dal file di Agostino',
                ],
                'user_id' => 2,
                'revision_of' => null
            ];

            $creds[] = Credential::create($cred);
        }

        return $creds;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'credential_id',
        'working'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'working' => 'boolean'
    ];

    public function credential()
    {
        return $this->hasOne('App\Credential');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}

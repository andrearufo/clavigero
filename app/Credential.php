<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'infos',
        'user_id',
        'revision_of',
        'private'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'infos' => 'array',
        'private' => 'boolean'
    ];

    protected $appends = [
        'revision',
        'score',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function getRevisionAttribute()
    {
        $revision = $this->where('revision_of', $this->id)->first();
        return ($revision) ? $revision->id : false;
    }

    public function getScoreAttribute()
    {
        $posivite = $this->votes->where('working', true)->count();
        $negative = $this->votes->where('working', false)->count();
        $sum = $posivite - $negative;

        return [
            'positive' => $posivite,
            'negative' => $negative,
            'sum' => $sum
        ];
    }
}
